package com.example.betvictor.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class TalkControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void testIndex() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/").contentType(MediaType.APPLICATION_JSON)).andReturn();
        Assertions.assertEquals("index", mvcResult.getModelAndView().getViewName());
    }

    @Test
    void testMonitor() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/action-monitor").contentType(MediaType.APPLICATION_JSON)).andReturn();
        Assertions.assertEquals("monitor", mvcResult.getModelAndView().getViewName());
    }

}

FROM openjdk:17-jdk-alpine
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar

ENV custom.version mock_value

expose 8080

CMD java -jar /app.jar

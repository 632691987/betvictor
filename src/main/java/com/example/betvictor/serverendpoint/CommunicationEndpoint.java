package com.example.betvictor.serverendpoint;

import com.example.betvictor.dto.MessageDTO;
import com.example.betvictor.service.MessageService;
import com.example.betvictor.util.AppUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

@ServerEndpoint("/webSocket/{senderId}")
@Component
@Slf4j
public class CommunicationEndpoint {
    private static final Map<String, User> userMap = new ConcurrentHashMap<>();

    private static User actionMonitor = null;

    @OnOpen
    public void onOpen(@PathParam(value = "senderId") String senderId, Session session) {
        if (senderId.equals("action-monitor")) {
            actionMonitor = new User(senderId, session);
        } else {
            userMap.put(senderId, new User(senderId, session));
        }
        log.info("senderId = {} open connection", senderId);
    }

    @OnClose
    public void onClose(@PathParam(value = "senderId") String userId) {
        userMap.remove(userId);
        log.info("userId = {} close connection", userId);
    }

    @OnMessage
    public void onMessage(@PathParam(value = "senderId") String senderId, String message, Session session) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            MessageDTO messageWrapper = objectMapper.readValue(message, MessageDTO.class);
            String mess = messageWrapper.getMessage();
            String receiverId = messageWrapper.getDestination();

            User receiver = userMap.get(receiverId);
            if (Objects.nonNull(receiver)) {
                receiver.sendMessage(mess);
            }

            monitorMessage(senderId, receiverId, mess);

            saveMessage(senderId, receiverId, mess);
        } catch (IOException e) {
            log.error("Exception occure, error message = {}", e.getMessage());
        }
    }

    private void monitorMessage(String senderId, String receiverId, String mess) {
        if (Objects.nonNull(actionMonitor)) {
            try {
                actionMonitor.sendMessage(String.format("Sender id = %s send message to receiver id = %s, content = %s", senderId, receiverId, mess));
            } catch (IOException | RuntimeException e) {
                log.error("action-monitor send message error, it may cause action monitor close web page");
                actionMonitor = null;
            }
        }
    }

    private void saveMessage(String sender, String receiver, String mess) {
        MessageService messageService = AppUtil.getBean(MessageService.class);
        messageService.saveMessage(sender, receiver, mess);
    }

    @OnError
    public void onError(@PathParam(value = "userId") String userId, Session session, Throwable error) {
        log.error("userId = {} have error", userId);
        error.printStackTrace();
    }

}

package com.example.betvictor.repository;

import com.example.betvictor.entity.Message;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

@DataJpaTest
class MessageRepositoryTest {

    @Autowired
    private MessageRepository messageRepository;

    @Test
    void testSave() {
        Message message = new Message();
        message.setReceiver("receiver");
        message.setSender("sender");
        message.setContent("content");
        message = messageRepository.save(message);

        Optional<Message> optional = messageRepository.findById(message.getId());
        Assertions.assertTrue(optional.isPresent());

        Message savedMessage = optional.get();
        Assertions.assertEquals(message.getReceiver(), savedMessage.getReceiver());
        Assertions.assertEquals(message.getContent(), savedMessage.getContent());
        Assertions.assertEquals(message.getSender(), savedMessage.getSender());
    }

}

package com.example.betvictor.serverendpoint;

import com.example.betvictor.dto.MessageDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;

import javax.websocket.ContainerProvider;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;
import java.net.URI;
import java.util.concurrent.TimeUnit;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class WebSocketTestScenario {

    @LocalServerPort
    private int randomPort;

    private WebSocketContainer container;

    private ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    public void setup() {
        container = ContainerProvider.getWebSocketContainer();
    }

    /**
     * Use two client connect to socket server, and then send message to each other
     *
     */
    @Test
    void testOneOnOneConnect() throws Exception {


        // Given
        String user1Id = "client1";
        String user2Id = "client2";
        String monitorId = "action-monitor";

        String client1Message = "this is message from client1";
        String client2Message = "this is message from client2";

        TestWebSocketClient client1 = new TestWebSocketClient();
        TestWebSocketClient client2 = new TestWebSocketClient();
        TestWebSocketClient monitor = new TestWebSocketClient();

        MessageDTO messageDTO1 = new MessageDTO();
        messageDTO1.setMessage(client1Message);
        messageDTO1.setDestination(user2Id);
        MessageDTO messageDTO2 = new MessageDTO();
        messageDTO2.setMessage(client2Message);
        messageDTO2.setDestination(user1Id);

        Session session1 = container.connectToServer(client1, URI.create("ws://localhost:" + randomPort + "/webSocket/" + user1Id));
        Session session2 = container.connectToServer(client2, URI.create("ws://localhost:" + randomPort + "/webSocket/" + user2Id));
        container.connectToServer(monitor, URI.create("ws://localhost:" + randomPort + "/webSocket/" + monitorId));





        // When
        session1.getBasicRemote().sendText(objectMapper.writeValueAsString(messageDTO1));
        TimeUnit.SECONDS.sleep(1);
        session2.getBasicRemote().sendText(objectMapper.writeValueAsString(messageDTO2));
        TimeUnit.SECONDS.sleep(1);





        // Then
        Assertions.assertEquals(client2Message, client1.getReceivedMessage());
        Assertions.assertEquals(client1Message, client2.getReceivedMessage());
        Assertions.assertFalse(monitor.getReceivedMessage().isEmpty());
    }

}

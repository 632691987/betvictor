package com.example.betvictor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BetVictorApplication {

    public static void main(String[] args) {
        SpringApplication.run(BetVictorApplication.class, args);
    }

}

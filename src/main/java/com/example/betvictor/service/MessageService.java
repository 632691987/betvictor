package com.example.betvictor.service;

import com.example.betvictor.entity.Message;
import com.example.betvictor.repository.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MessageService {

    @Autowired
    private MessageRepository messageRepository;

    public void saveMessage(String sender, String receiver, String content) {
        Message message = new Message();
        message.setContent(content);
        message.setSender(sender);
        message.setReceiver(receiver);
        messageRepository.save(message);
    }

}

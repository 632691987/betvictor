package com.example.betvictor.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class InfoControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void testRestfulStatus() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/status").contentType(MediaType.APPLICATION_JSON)).andReturn();
        Assertions.assertEquals("OK", mvcResult.getResponse().getContentAsString());
    }

    @Test
    void testRestfulVersion() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/version").contentType(MediaType.APPLICATION_JSON)).andReturn();
        Assertions.assertFalse(mvcResult.getResponse().getContentAsString().isEmpty());
    }

}

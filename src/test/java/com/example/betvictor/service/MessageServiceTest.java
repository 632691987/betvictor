package com.example.betvictor.service;


import com.example.betvictor.entity.Message;
import com.example.betvictor.repository.MessageRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MessageServiceTest {

    @Mock
    private MessageRepository messageRepository;

    @InjectMocks
    private MessageService messageService;

    @Test
    void testSaveMessage() {
        when(messageRepository.save(any(Message.class))).thenReturn(null);
        messageService.saveMessage("a sender", "a receiver", "a content");

        verify(messageRepository).save(any(Message.class));
    }

}

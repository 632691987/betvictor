package com.example.betvictor.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping()
public class TalkController {

    @GetMapping
    public String indexPage() {
        return "index";
    }

    @GetMapping("action-monitor")
    public String monitorPage() {
        return "monitor";
    }

}

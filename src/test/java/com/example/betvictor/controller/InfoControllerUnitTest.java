package com.example.betvictor.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

class InfoControllerUnitTest {

    InfoController infoController = new InfoController();

    @Test
    void testStatus() {
        Assertions.assertEquals("OK", infoController.getStatus());
    }

    @Test
    void testVersion() {
        Assertions.assertNull(infoController.getVersion(), "The version should be transfer dynamic by docker-compose");
    }
}

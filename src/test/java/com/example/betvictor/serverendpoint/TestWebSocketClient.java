package com.example.betvictor.serverendpoint;

import javax.websocket.ClientEndpoint;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;

@ClientEndpoint
public class TestWebSocketClient {
    private String receivedMessage = "";

    @OnOpen
    public void onOpen(final Session session){
    }

    @OnMessage
    public void onMessage(@PathParam(value = "senderId") String senderId, String message, Session session) {
        receivedMessage = message;
    }

    public String getReceivedMessage() {
        return receivedMessage;
    }
}

package com.example.betvictor.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class TalkControllerUnitTest {

    TalkController talkController = new TalkController();

    @Test
    void testIndex() {
        Assertions.assertEquals("index", talkController.indexPage());
    }

    @Test
    void testMonitor() {
        Assertions.assertEquals("monitor", talkController.monitorPage());
    }

}

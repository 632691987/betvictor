package com.example.betvictor.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InfoController {

    @Value("${custom.version}")
    private String version;

    @GetMapping(value = "status", produces = MediaType.APPLICATION_JSON_VALUE)
    public String getStatus() {
        return "OK";
    }

    @GetMapping(value = "version", produces = MediaType.APPLICATION_JSON_VALUE)
    public String getVersion() {
        return version;
    }

}
